---
title: Omgur
location: https://imgur.alt.tyil.nl
upstream: https://github.com/geraldwuhoo/omgur
---

This service does not implement a "front page", but actual links to images or
albums should work fine.
