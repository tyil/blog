---
title: Searx
location: https://searx.tyil.nl
upstream: https://github.com/searx/searx
---

Searx is a free internet metasearch engine which aggregates results from more
than 70 search services. Users are neither tracked nor profiled.
