---
title: Fiche
location: https://p.tyil.nl
upstream: https://github.com/solusipse/fiche
---

Fiche is used for hosting pastes, which can then be accessed over HTTP at
`p.tyil.nl`. The easiest way to create a new paste is with `nc`.

To paste the output of a given command:

```
$command | nc tyil.nl 9999
```

Or, to upload the contents of a file:

```
nc tyil.nl 9999 < file
```

The buffer size is set to 32k, so that is the maximum size you can paste to it.
