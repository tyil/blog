---
date: 2021-05-22
title: Raku is moving to Libera.chat
tags:
- Raku
- LiberaChat
- IRC
social:
  email: mailto:~tyil/public-inbox@lists.sr.ht&subject=Raku is moving to Libera.chat
---

Earlier this week, the staff at the Freenode IRC network have resigned en
masse, and created a new network, Libera. This was sparked by [new ownership of
Freenode](https://kline.sh/). Due to concerns with the new ownership, the Raku
Steering Council has decided to also migrate our IRC channels to Libera.

This requires us to take a couple steps. First and foremost, we need to
register the Raku project, to allow us a claim to the `#raku` and related
channels. Approval for this happened within 24 hours, and as such, we can
continue on the more noticable steps.

The IRC bots we're using for various tasks will be moved next, and the Raku
documentation has to be updated to refer to Libera instead of Freenode. The
coming week we'll be working on that, together with the people who provide
those bots.

Once this is done, the last step involves the Matrix bridge. Libera and
Matrix.org staff are working on this, but there's no definite timeline
available just yet. This may mean that Matrix users will temporarily not be
able to join the discussions happening at Libera. We will keep an eye on the
progress of this, and set up the bridge as soon as it has been made available.

If you have any questions regarding the migration, feel free to reach out to us
via email (`rsc@raku.org`) or on IRC (`#raku-steering-council` on
irc.libera.chat).
