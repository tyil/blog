---
title: Lately in Raku
date: 2020-06-21
tags:
- Raku
---

I've been working on some Raku projects, but each of them is *just* too small
to make an individual blog post about. So, I decided to just pack them together
in a slightly larger blog post instead.

## Binary Rakudo Star builds for GNU+Linux and FreeBSD

A friend on IRC asked if it was possible to get Rakudo Star precompiled for
ARM, since compiling it on his machine took forever. I took a look around for
potential build services, and settled for [Sourcehut](https://builds.sr.ht/).

I added build instructions for amd64 FreeBSD, GNU+Linux, musl+Linux, and ARM
GNU+Linux. Tarballs with precompiled binaries get build whenever I push to the
Rakudo Star mirror on Sourcehut, and are uploaded to
[dist.tyil.nl/tmp](https://dist.tyil.nl/tmp/). Currently, these are not
considered to be an official part of Rakudo Star, but if interest increases and
more people can test these packages, I can include them in official releases.

## `IRC::Client` plugins

IRC bots are great fun, and the
[`IRC::Client`](https://github.com/raku-community-modules/perl6-IRC-Client)
module allows for easy extension through *plugins*. For my own IRC bot,
[musashi](https://git.sr.ht/~tyil/raku-local-musashi), I've created two new
plugins, which are now available in the Raku ecosystem for anyone to use.

### `IRC::Client::Plugin::Dicerolls`

The first plugin I've created can do dice rolls, D&D style. You can roll any
number of dice, with any number of sides, and add (or subtract) bonusses from
these.

```txt
<@tyil> .roll 1d20
<+musashi> 1d20 = 1
<@tyil> .roll 5d20
<+musashi> 5d20 = 3 + 19 + 8 + 6 + 11 = 47
<@tyil> .roll 1d8+2d6+10
<+musashi> 1d8+2d6+10 = 4 + 6 + 4 + 10 = 24
```

Since this is ripe for abuse, the plugin allows to set limits, and sets some
defaults for the limits as well. This should help prevent your bot from getting
killed for spam.

### `IRC::Client::Plugin::Reminders`

Everyone forgets things, and there's various tools helping people remember
things in various situations. For IRC based situations, I created a reminder
plugin for `IRC::Client`.

```txt
10:19 <@tyil> musashi: remind me to write a blog post in 10 minutes
10:19 <+musashi> Reminding you to write a blog post on 2020-06-21T08:29:00Z (UTC)
10:29 <+musashi> tyil: Reminder to write a blog post
```

It's not very sophisticated yet, working only with numbers and certain
identifiers (minutes, hours, days, weeks), but I may add more useful
identifiers later on such as "tomorrow", or "next Sunday". Contributions for
such extended functionality are obviously also very welcome!

There's [a small
issue](https://git.sr.ht/~tyil/raku-irc-client-plugin-reminders/tree/master/lib/IRC/Client/Plugin/Reminders.rakumod#L69)
with logging in a `start` block. It seems the dynamic variable `$*LOG` is no
longer defined within it. If anyone has an idea why, and how I could fix this,
please let me know!

## Template program for D&D

Another little utility I made for D&D purposes. My DM asked me how hard it'd be
to create a program to fill out a number of templates he made, so he could use
them in the game with another party. He was able to hand me a list of variables
in the form of a CSV, so I set out to use that. With some help from `Text::CSV`
and `Template::Mustache`, I had a working solution in a couple minutes, with
all the required things nicely fit into a single file.

I had not used `$=pod` before in Raku, and I'm quite happy with how easy it is
to use, though I would like a cleaner way to refer to a Pod block by name.

```raku
#!/usr/bin/env raku

use v6.d;

use Template::Mustache;
use Text::CSV;

#| Read a CSV input file to render contracts with.
sub MAIN () {
        # Set the directory to write the contracts to.
        my $output-dir = $*PROGRAM.parent(2).add('out');

        # Make sure the output directory exists
        $output-dir.mkdir;

        # Load the template
        my $template = $=pod
                .grep({ $_.^can('name') && $_.name eq 'template' })
                .first
                .contents
                .map(*.contents)
                .join("\n\n")
                ;

        # Parse STDIN as CSV
        my @records = Text::CSV
                .new
                .getline_all($*IN)
                .skip
                ;

        # Create a contract out of each record
        for @records -> @record {
                $output-dir.add("contract-{@record[0]}.txt").spurt(
                        Template::Mustache.render($template, {
                                contractor => @record[2],
                                date => @record[1],
                                description => @record[6],
                                item => @record[3],
                                location => @record[5],
                                price => @record[4]
                        }) ~ "\n"
                );
        }
}

=begin template
As per our verbal agreement this contract will detail the rewards, rights, and
obligations of both parties involved.

The contractor, to be known henceforth as {{ contractor }}.
The contractee, to be known henceforth as the Association.

{{ contractor }} requests the delivery of an object identified as the "{{ item }}"
to be delivered by the Association at the location specified for the agreed
upon compensation. The Association shall deliver the object within two weeks of
the signing of this contract and receive compensation upon delivery.

The location is to be known as "{{ location }}", described as "{{ description }}".
The compensation agreed upon is {{ price }} pieces of Hondia standard
gold-coin currency, or an equivalent in precious gemstones.

Written and signed on the {{ date }}.

For the association, Lan Torrez
For the {{ contractor }}
=end template
```
