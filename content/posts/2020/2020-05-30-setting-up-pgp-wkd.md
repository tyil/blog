---
date: 2020-05-30
title: Setting Up a PGP Webkey Directory
tags:
- PGP
- GPG
- WKD
- Security
aliases:
- /post/2020/05/30/setting-up-pgp-wkd/
---

A little while ago, a friend on IRC asked me how I set up a PGP webkey
directory on my website. For those that don't know, a webkey directory is a
method to find keys through `gpg`'s `--locate-key` command. This allows people
to find my key using this command:

```txt
gpg --locate-key p.spek@tyil.nl
```

This is a very user-friendly way for people to get your key, as compared to
using long IDs.

This post will walk you through setting it up on your site, so you can make
your key more easily accessible to other people.

## Set up the infrastructure

For a webkey directory to work, you simply need to have your key available at a
certain path on your website. The base path for this is
`.well-known/openpgpkey/`.

```sh
mkdir -p .well-known/openpgpkey
```

The webkey protocol will check for a `policy` file to exist, so you must create
this too. The file can be completely empty, and that's exactly how I have it.

```sh
touch .well-known/openpgpkey/policy
```

The key(s) will be placed in the `hu` directory, so create this one too.

```sh
mkdir .well-known/openpgpkey/hu
```

## Adding your PGP key

The key itself is just a standard export of your key, without ASCII armouring.
However, the key does need to have its file **name** in a specific format.
Luckily, you can just show this format with `gpg`'s `--with-wkd-hash` option.

```sh
gpg --with-wkd-hash -k p.spek@tyil.nl
```

This will yield output that may look something like this:

```txt
pub   rsa4096/0x7A6AC285E2D98827 2018-09-04 [SC]
      Key fingerprint = 1660 F6A2 DFA7 5347 322A  4DC0 7A6A C285 E2D9 8827
uid                   [ultimate] Patrick Spek <p.spek@tyil.nl>
                      i4fxxwcfae1o4d7wnb5bop89yfx399yf@tyil.nl
sub   rsa2048/0x031D65902E840821 2018-09-04 [S]
sub   rsa2048/0x556812D46DABE60E 2018-09-04 [E]
sub   rsa2048/0x66CFE18D6D588BBF 2018-09-04 [A]
```

What we're interested in is the `uid` line with the hash in the local-part of
the email address, which would be `i4fxxwcfae1o4d7wnb5bop89yfx399yf@tyil.nl`.
For the filename, we only care about the local-part itself, meaning the export
of the key must be saved in a file called `i4fxxwcfae1o4d7wnb5bop89yfx399yf`.

```sh
gpg --export 0x7A6AC285E2D98827 > .well-known/openpgpkey/hu/i4fxxwcfae1o4d7wnb5bop89yfx399yf
```

## Configuring your webserver

Lastly, your webserver may require some configuration to serve the files
correctly. For my blog, I'm using [`lighttpd`](https://www.lighttpd.net/), for
which the configuration block I'm using is as follows.

```lighttpd
$HTTP["url"] =~ "^/.well-known/openpgpkey" {
	setenv.add-response-header = (
		"Access-Control-Allow-Origin" => "*",
	)
}
```

It may be worthwhile to note that if you do any redirection on your domain,
such as adding `www.` in front of it, the key lookup may fail. The error
message given by `gpg` on WKD lookup failures is... poor to say the least, so
if anything goes wrong, try some verbose `curl` commands and ensure that the
key is accessible at the right path in a single HTTP request.

## Wrapping up

That's all there's to it! Adding this to your site should be relatively
straightforward, but it may be a huge convenience to anyone looking for your
key. If you have any questions or feedback, feel free to reach out to me!
