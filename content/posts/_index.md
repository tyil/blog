---
title: Blog
---

Over time, I've written a number of blog posts. Some to voice my opinion, some
to help people out with a tutorial, or just because I found something fun or
interesting to talk about. You can find these blog posts on this section of my
site. If you have any comments, feel free to send an email to
[`~tyil/public-inbox@lists.sr.ht`](mailto:~tyil/public-inbox@lists.sr.ht), or
reach out through any other method listed on the home page.

{{< admonition title="note" >}}
**These articles reflect my opinion, and only mine**. Please refrain from
accusing other people of holding my opinion for simply being referenced in my
articles.
{{< / admonition >}}
