---
title: The Perl Conference in Glasgow
date: 2018-08-23
tags:
- Conference
- Perl
---

This year the European Perl Conference was hosted in Glasgow, and of course
I've attended a number of presentations there. On some of these, I have some
feedback or comments. These talks, and the feedback I have for them, are
detailed in this blog post.

{{< admonition title="note" >}}
The first talk I cover is not so much about Perl, but more about politics, as
the talk was mostly about the speaker's ideology. If this does not interest you,
I'd suggest you skip the [Discourse Without Drama](#discourse-without-drama)
section, and head straight to the [European Perl Mongers Organiser's Forum
2018](#european-perl-mongers-organiser-s-forum-2018).
{{< / admonition >}}

## Discourse Without Drama

This was the first talk, and the only talk available at this timeslot. I am
personally very much against the diversity ideology, and must admit I am
skeptical of such presentations from the get-go. Nonetheless, I did stay until
the end and tried to give it a fair shot.  However, I cannot sit idle while she
tries to force her ideology on this community I care very deeply about.

{{< admonition title="note" >}}
I am not against the concept of diversity, I wholly support the idea of equal
opportunities. What I do not accept is the idea of equal outcome, or forced
diversity based on physical traits. This is what I refer to with "the diversity
ideology". I also don't think anyone has a right not to be offended, as this is
impossible to achieve in the real world.
{{< / admonition >}}

One of the things that stood out to me is that the speaker tells us not to use
logical fallacies to condemn her ideology. This on itself I can easily agree
with. However, this should go both ways: we should also not use logical
fallacies to promote her ideology. Most notably, she pointed out the
[_argumentum ad populum_](https://en.wikipedia.org/wiki/Argumentum_ad_populum).
This basically means that just because a lot of people do or say something,
doesn't make it right. And this applies to the idea that we need to push the
diversity ideology in the Perl community as well. Try to bring facts and
statistics to show that this ideology will actually improve the community in
the long term. I've personally not seen any community improve with increasingly
harsh punishments for increasingly minor offenses.

Another thing which slightly bothered me is the useless spin into radical
feminist ideology, which to me seems very off-topic for a Perl conference.
We're not at a political rally, and these kinds of remarks have been very
divisive in all sorts of other environments already. I'd rather not bring this
kind of behaviour to a community which I have loved for being so incredibly
friendly without needing special rules and regulations for it.

Next, a point is raised that people should *not* grow a thicker skin. Instead,
people should get softer hearts. While I can get behind the latter part, I
have to disagree with the former. Reality shows that communications don't
always go perfectly. This is even more true in a community that exists mostly
in the digital space. Context is often lost here, and that can lead to
situations where someone may feel attacked even if this was not the intention
at all. I can safely say I've been in many situations where my comments were
perceived as an attack when they were not ment to be.

People need to be able to handle some criticism, and sometimes you'll just have
to assume good faith from the other party. Telling people they should never
have to consider context and have a right not to be offended fosters an
environment in which people will be afraid to give genuine, valid feedback.

She seemed very much in favour of an overly broad code of conduct as well, of
which I am also a strong opponent. There are various articles online, such as
[this one](https://shiromarieke.github.io/coc.html), which show that just
slapping a generic, vague code of conduct to a community isn't going to solve
the issue of trolls or harmful behaviour. There's [another great
article](http://quillette.com/2017/07/18/neurodiversity-case-free-speech/) that
I was pointed towards that highlight how this attempt to censor people for the
sake of not offending anyone can effectively halt creativity and the exchange of
ideas. There was also an interesting quote written on one of the walls of the
venue:

{{< quote attribution="Oscar Romero" >}}
Aspire not to have more, but to be more...
{{< / quote >}}

Don't try to add meaningless documents such as a code of conduct, which more
often than not hurts a community instead of improving it. Try to be a better
person that tries to solve actual issues without harming the community at large.
Be the adult in the conversation that can take an insult, and still be kind.
[Remember to hug the
trolls](https://rakudo.party/post/On-Troll-Hugging-Hole-Digging-and-Improving-Open-Source-Communities#hug2:feedthehandthatbitesyou),
and eventually they will hug you back.

## European Perl Mongers Organiser's Forum 2018

The Perl community isn't big nowadays, however, the Perl 6 language also offers
a lot of concepts which are very well suited for modern programming.  Sadly, if
no new users try out the language, it will be all for nothing. As such, we need
to bring new blood in to the community.

One of the ways of doing this is by extending our promoting efforts outside of
the Perl community. Most people who like Perl are in a social bubble with other
people that are also familiar with the Perl programming language, be it 5 or 6.
But we need to reach new people as well, who will most likely be outside of
this social bubble. These people don't have to be techies either, they might
just as well be marketeers or designers.

I myself am part of the "techies", so I'll stick to this particular group for
now. And I know people like me can be found at meetups, so it would be
worthwhile to promote Perl at meetups which are not dedicated to Perl. Think of
more generic programming meetups, or GNU+Linux User Groups. We have to be
mindful not to be too pushy, though. Listen to other people, and try to
understand the problem they're facing. Most of them will not be open to using a
different language immediately, especially not Perl (which sadly has a
particularly bad standing amongst people unfamiliar with it). Try to assist
them with their issues, and slowly introduce them to Perl (6) if it helps to
showcase what you mean. It might also be interesting to show people examples on
how to solve certain issues before telling them the language's name, so they
don't have a negative preconception solely from the name.

Another thing to note is that Perl is more than just a programming language.
It's a community, and a large library of modules, known as CPAN. And CPAN
offers some nifty tools, such as the CPAN testers, which help ensure module
developers that their code runs on a massive set of platforms and Perl
versions.

This has led me to consider the creation of a new Perl 6 module:
`CPAN::Tester`, to make it easy for people to contribute to a large-scale
testing environment for Perl 6. The idea is that one can run `CPAN::Tester` on
their machine, which will keep track of new Perl 6 modules being uploaded to
CPAN. The results are to be sent to another server (or multiple servers), which
can aggregate the data and show a matrix of test results. This aggregating
server could also be built as a Perl 6 module, possibly named
`CPAN::Tester::ResultsServer`. This would make setting up an environment
similar to CPAN testers for Perl 5 quite easy for Perl 6.

## Perl 6 in Real Life $Work

The speaker shows the perfect use case for
[Perl 6 grammars](https://docs.perl6.org/language/grammars), advanced yet
readable parsing of text and performing actions with the results. It's an
interesting talk, showcasing some nifty grammar constructs. The best part of
this is that it actually runs in production, where it parses over 700 files,
consisting over 100,000 lines of code, in about 22 seconds (on his laptop).
This goes to show that Perl 6 is no longer "too slow to use in production".

It might be interesting to run this application of grammars on every Perl 6
release to gather more information on the speed improvements of Perl 6, much
like Tux's `Text::CSV` runs.

## Releasing a Perl 6 Module

The speaker starts off with detailing the platform which most Perl 6 modules
use to host their code repository, GitHub. He also touched upon automated
testing using Travis and AppVeyor. It was good to show how to make use of
these, as automated testing oftentimes stops unintended bugs from reaching end
users. But, I personally prefer GitLab over GitHub, as they have much better
testing functionality, and they actually release their own platform as an open
source package. I'd like more GitLab love from the community and speakers as
well if possible. This would also make the speaker's CI configuration simpler,
for which he currently uses a `.travis.yml` file.  This requires him to build
Perl 6 from source every test run, wasting quite a lot of time.

It was also noted that there's a module to help you set up this module
skeleton, `mi6`. The speaker also noted that it doesn't seem to add much once
you know how a Perl 6 module is organized, and I tend to agree with this.
Actually, I made a module precisely because I agree with him here,
`App::Assixt`. This module intends to smoothen the entire course of module
development, not just the creation of a skeleton file. It will take care of
keeping your `META6.json` up to date, and ease uploading your module to CPAN as
well.

Lastly, the speaker says the `META6.json` documentation can be found in S22.
While this is technically correct, S22 is *not* the implementation's
documentation, this lives in the official Perl 6 documentation instead. S22
offers many additional information to be stored in the `META6.json`, but using
these fields will actually break installation of your module through `zef`,
rendering it unusable by others. I would strongly recommend people not to use
S22 when trying to figure out what they can or cannot do with their
`META6.json`.

## How to become CPAN contributor?

Submitting a pull request (or more correctly named, merge request) to a
repository is possibly the most straightforward way to help out other projects.
However, sometimes it will take a long time to get a response. The speaker
notes this can actually be on the scale of years. I have authored a number of
modules myself, and have been in the situation where I had not realized I got a
merge request from another person (same goes for issue reports). I would
recommend people who are not getting timely responses to their contributions to
contact the maintainer via other channels which are more suited for
communications. Think of email or IRC, for instance. You'll generally have a
much better chance of getting a timely response from the author, and then you
can work out your contribution and see if you can get it merged into the main
project.

The speaker also lists a couple of ways to get started with contributing to
modules. One thing I missed in particular was the Squashathons[^1] for Perl 6.
These generally offer a good entry point to help out with the language's
development and the ecosystem's maintainance.

Near the end, it was pointed out that it is a good idea to have a thick skin.
Even when it's not intended, people can come accross as rude. This is in
opposition to the talking point of the speaker yesterday (_Discourse Without
Drama_), but he does raise a good point here. People oftentimes don't mean to
insult you, but context is easily lost in written communications. Try to stay
mature and professional, you can simply ask for clarification. If you feel the
person remains hostile towards you, walk away. There's plenty of other projects
that would love your contributions!

## Conference Organizers & European Perl Mongers Organiser's Forum 2018 BoF

Well, that's certainly a mouthful for a heading, and it even contains an
abbreviation! This event was not a presentation, but a platform to exchange
ideas together.

One of the items that were up for discussion was _A Conference Toolkit_, or ACT
for short.  This is the platform used to organize Perl events, such as this
conference and Perl workshops throughout the world. However, ACT is dated.
They enabled HTTPS a short while ago, but it's still not the default because
people don't want to risk breaking the platform. I think this is enough of
an indication that it might be time to make something new to replace it.

And I'm not alone in that sentiment, it seems. However, ACT is big and contains
a lot of data we don't want to lose. It's a massive undertaking to make a new
tool that works at least as well, and allows us to make use of the old data as
well. There is a Trello board available that lists all the features that would
be required to implement, so that's a good start already. I think now it needs
a dedicated product owner with people contributing code, so a start can be
made. This does seem like a touchy subject, since I'm far from the first person
to want this. Many before me have tried and failed already.

As such, I'd propose not making it a Perl centric tool. Make it a modular,
generic event organizing tool. Get a good database design that we can import
our old data into, so nothing is lost, but things can be converted to be more
useful for our current needs. This way, we can work in small steps, and maybe
even reach contributors from outside the regular Perl circles. This might even
bring in new partnerships (or sponsors) towards the Perl community.

Personally, I'd like to see something like this to be written in Perl 6. This
way, it could also be used as a showcase project for the Perl 6 programming
language.

## Writing a Perl 6 Module

Perl 6 has this very neat feature called
[subsets](https://docs.perl6.org/language/typesystem#index-entry-subset-subset).
These can be used to make your own types with very little effort, which can
help tremendously to keep your code clean and concise. There are two arguments
I have in favour of subsets that the speaker did not touch upon.

First off, using a subset instead of a `where` clause in a sub or method
signature will bring much better error messages. If you use a `where` in your
signature, and the check fails, you'll get an error that there was no signature
that matched `where { ... }`.

Secondly, if you want to use abstract methods, you can't really use a `where`.
[I'ev asked a question about this on Stack
Overflow](https://stackoverflow.com/questions/51570655/how-to-use-abstract-multi-methods-containing-a-where),
which has the details as to why this doesn't work the way you might expect.

Next, there's some cool things about operators in Perl 6. There are many of
these available by default, and it's _very_ easy to add new ones yourself as
well. In fact, the `Math::Matrix` module used throughout the presentation makes
some available as well. Thanks to the ease of adding operators in Perl 6, if
you have a `Math::Matrix $m` in Perl 6, you can get the norm by writing `|| $m
||`. This is the mathematically correct way to write this, making it easy to
understand for everyone using matrixes in their daily lives. If you're a
mathematician, small things like these are great to have.

I have some comments on the `Math::Matrix` module itself as well, based on
slides shown in the presentiation. The first thing I noticed is that there's a
`norm` method using a `where` clause when it's not needed:

```raku
method norm (Str $which where * eq 'row-sum')
```

This can be written instead as:

```raku
method norm ('row-sum')
```

This is shorter and clearer, and you'll get better feedback from the compiler as
well. I [submitted a pull request on the GitHub
repository](https://github.com/pierre-vigier/Perl6-Math-Matrix/pull/49) in an
attempt to improve this, which got merged! The speaker was not aware it could be
done in this manner, so I'm proud I got to teach him something right after he
did his presentation.

## Winding down

I've had a great time at the Perl conference, spoke to many people with whom
I've had some great discussions. I got to meet and personally thank a number of
people who've helped me out over the past year as well.

A big thank you to all the people who made this conference possible, and I hope
to see you all again in Riga!

[^1]: A Squashathon is like a hackathon, except everyone in the world is
invited, and you can help out over the Internet, staying in your own home. Of
course, you can still meet up with other developers and make it a social
gathering in the real world as well!
