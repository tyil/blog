---
title: The SoC Controversy
date: 2019-08-10
tags:
- CodeOfConduct
- Conference
- Perl6
- Raku
---

{{< admonition title="Disclaimer" >}}
Please keep in mind that the opinion shared in this blog post is mine and mine
alone. I do not speak for any other members of the PerlCon organization team.
Please do not address anyone but me for the positions held in this post.
{{< / admonition >}}

Those that know me are probably aware that I generally dislike to make
political posts on my personal blog. I'd rather stick to technological
arguments, as there's less problems to be found with regards to personal
feelings and all that. However, as I'm growing older (and hopefully more
mature), I find it harder to keep politics out of my life as I interact with
online communities. This becomes especially true as I plan to assist with
organizing [PerlCon
2020](https://wiki.perlcon.eu/doku.php/proposals/2020/amsterdam).

PerlCon 2019 ended yesterday, and I had a lot of fun. I'd like to thank the
organizer, Andrew Shitov, once more for doing an amazing job. Especially so, as
he has been harassed for weeks, for trying to organize the conference. The
reason behind the harassment was partly due to his decision to not have an SoC,
or "Standards of Conduct", for PerlCon 2019.

During his final announcements at the end of the conference, he noted that this
is still happening, even in person at the conference itself. This toxic
behavior towards him has made him decide to no longer involve himself in
organizing a conference for the Perl community. I personally think this is a
loss for everyone involved in the community, and one that was completely
avoidable by having humane discussion instead of going for Twitter harassment.

For what it's worth, I think Twitter is also the worst possible place on the
Internet for any reasonable discussion, as it puts a very low limit on the
amount of characters you are allowed to spend on a single post. This makes it
downright impossible for any discussion, and seems to always lead to petty
name-calling. This is one of the reasons why [I'm instead using a Pleroma
instance](https://soc.fglt.nl/main/public) for my social media presence on the
Internet. If anyone is on the Internet with the intent of having interesting
discussion, I'd highly recommend to use some entrance into the Fediverse. The
instance I'm using is open for sign-ups!

But I digress. The SoC controversy is what made me want to write this blog
post. I wonder why this even is a controversy. Why do people think it is
impossible to co-exist without some document describing explicitly what is and
is not allowed? I would hope that we're all adults, and can respect one another
as such.

I wonder, was there any certain event at PerlCon 2019 that would've been
avoided if there *was* a SoC provided? I certainly did not, at any point, feel
that people were being harmful to one another, but maybe I'm just blind to it.
If anyone has concrete examples of events that happened during PerlCon 2019
that a SoC could've prevented, I would be genuinely interested in hearing about
them. If I am to assist in organizing PerlCon 2020, and I want to be able to
present a good argument on the SoC discussion, I'll need concrete examples of
real problems that have occurred.

Of course, I also consider the opposite of this discussion. Can the SoC be used
to *cause* harm, in stead of deter it? For this, I actually have clear
evidence, and the answer is a resounding **yes**. The harassment brought upon
Andrew was originally caused by an event that transpired at The Perl Conference
in Pittsburgh (2019). A video was removed, and a speaker harassed, for
dead-naming someone. Until that event, I wasn't even aware of the term, but
apparently it's grounds for removal of your presentation from the conference
archives.

A similar event happened with The Perl Conference in Glasgow (2018), where a
talk was also removed from the archives for a supposedly offensive joke that
was made. This also sparked a heavy discussion on IRC back then, with people
from all sides pitching in with their opinion.

From my perspective, the people shouting the loudest in these discussions
aren't interested in making the world a better place where we can live in
harmony, but to punish the offender for their behavior. I don't think we
should strive towards punishment, but towards understanding, if anything. Just
being angry, shouting at people (either in real life, or over the Internet)
isn't going to solve any underlying problem. It is more likely to cause more
issues in the long run, where people will just be more divided, and will want
to get continuous revenge upon the other side.

Additionally, I think that the existence of an SoC or likewise document is a
sign towards outsiders that your community can't behave itself maturely. They
need special rules laid out to them, after all. Like most rules, they are
codified because issues have arisen in the past, and keep on arising. I don't
think the Perl community is too immature to behave itself. I trust in the good
faith of people, and to me it feels like a SoC does the exact opposite.

I hope this blog post does it's job to invite you kindly to share your opinions
with me, either on [IRC, email or on the Fediverse](/#contact). I'd
gladly start a discussion on the positive and negative effects the SoC has, and the problems
it solves and creates. I think a civil discussion is in order here, to best
prepare us for PerlCon 2020.
