---
title: How to sign PGP keys
date: 2019-02-03
tags:
- PGP
- Tutorial
---

Having attended [FOSDEM](https://fosdem.org/2019/) last weekend, I have been
asked to help some people out with signing PGP keys. As it is an international
gathering of users and developers of all levels of expertise, it's a great event
to get your key out in to the wild. While helping people out, I figured it might
be even easier next time around to just refer to a small tutorial on my blog
instead.

## Creating a PGP key

The first step to sign keys, is to have a PGP key. If you already have one,
you're good to go to the next part of this tutorial. If you don't, you can check
out the `gpg` manual on how to create a key, or read about key creation in my
[article on using PGP with a Yubikey][yubikey-pgp-article]. While I would
strongly suggest reading at least some material, `gpg` does quite a good job of
guiding you through the process without prior knowledge, so you can just get
started with `gpg --generate-key` as well.

[yubikey-pgp-article]: {{ "/post/2018/09/04/setting-up-pgp-with-a-yubikey/#creating-pgp-keys" | prepend: site.baseurl | prepend: site.url }}

## Create key slips

A *key slip* is a small piece of paper containing some basic information about
the PGP key. They're exchanged when people meet, so they don't have to
immediately sign the key, but can do it safely at home. When you're signing in a
group, this may be faster to work with. Another benefit is that some people
don't have their private keys with them. They can then just collect the key slips
from the people who's key they want to sign, and sign it whenever they are in
possession of their private key again.

A key slip doesn't have to contain much. A key ID, fingerprint, email address and
a name is plenty. For reference, my key slips look as follows:

```txt
Patrick Spek <p.spek@tyil.nl>   rsa4096/0x7A6AC285E2D98827
    1660 F6A2 DFA7 5347 322A  4DC0 7A6A C285 E2D9 8827
```

## Verifying the owner

Before you sign anyone's public key, you should verify that the person is
actually who they say they are. You can easily do this by asking for government
issued identification, such as an ID card, driver's license or passport. What
constitutes good proof is up to you, but in general people expect at least one
form of government issued identification.

If the person can't verify who they are, you should *not* sign their key!

## Retrieving their key

Once you have verified the person is who they say they are, and you have
received their key slip containing their key ID, you can look up their key
online. You can let `gpg` do all the work for you in searching and downloading
the key, using the `--search` switch. For instance, to retrieve my key, do the
following:

```txt
gpg --search-keys 0x7A6AC285E2D98827
```

If a result has been found, you are prompted to enter the numbers of the keys
you want to download. Make sure you download the right key, in case multiple
have been found!

After retrieving the key, you can see it in the list of all the keys `gpg` knows
about using `gpg --list-keys`.

## Signing their key

To actually sign their key, and show that you trust that the key belongs to the
person's name attached to it, you can use `gpg --sign-key`:

```txt
gpg --sign-key 0x7A6AC285E2D98827
```

You will be prompted whether you are sure you want to sign. You should answer
this with a single `y` to continue.

After signing it, you'll have signed a PGP key! You can verify this by looking
at the signatures on a given key with `--list-sigs 0x7A6AC285E2D98827`. This should
contain your name and key ID.

## Exchanging the signed key

While you could publish the updated public key with your signature on it, you
should **not** do this! You should encrypt the updated public key and send it to
the person that owns the private key, and they should upload it themselves. One
reason for this is that it allows you to safely verify that they do in fact
actually own the private key as well, without ever asking them explicitly to
show you their private key.

To export the public key, use `--export`:

```txt
gpg --armor --export 0x7A6AC285E2D98827 > pubkey-tyil.asc
```

The `--armor` option is used to export the key as base64, instead of binary
data.

You can attach this file to an email, and let your email client encrypt the
entire email and all attachments for they key ID. How you can do this depends on
your email client, so you should research how to do this properly in the
documentation for it.

However, it's also possible to encrypt the public key file before adding it as
an attachment, in case you don't know how to let your email client do it (or if
you don't trust your email client to do it right).

You can use the `--encrypt` option for this, and add a `--recipient` to encrypt
it for a specific key.

```txt
gpg --encrypt --recipient 0x7A6AC285E2D98827 < pubkey-tyil.asc > pubkey-tyil.pgp
```

Now you can use this encrypted key file and share it with the owner of the key.
If the person you send it to really is the owner of the key, they can use the
private key to decrypt the file, import it with `gpg --import` and then publish
it with `gpg --send-keys`

## Winding down

Once all this is done, other people should have sent you your signed pubkey as
well, and you should have published your updated key with the new signatures.
Now you can start using PGP signatures and encryption for your communication
with the world. People who have not signed your key can see that there's other
people that do trust your key, and they can use that information to deduce that
whatever's signed with your key really came from you, and that anything they
encrypt with your public key can only be read by you.

With this [trust](https://en.wikipedia.org/wiki/Web_of_trust), you can make
communication and data exchange in general more secure.
