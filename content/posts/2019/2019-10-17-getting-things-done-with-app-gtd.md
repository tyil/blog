---
title: "Getting Things Done with App::GTD"
date: 2019-10-07
tags:
- GettingThingsDone
- Perl6
- Raku
---

A couple months ago, I was given a workshop at work about "getting things
done". There I was told that there exists a concept called "[Getting Things
Done](https://en.wikipedia.org/wiki/Getting_Things_Done)", or "GTD" for short,
to help you, well, get things done. A number of web-based tools were introduced
to assist us with following the rules laid out in the concept.

## The problem

The tools that were introduced did their job, and looked reasonably shiny.
However, most required a constant Internet connection. I like my tools to be
available offline, and optionally synced together. There was one local
application and a couple cloud-synced applications that I found, so this
problem could've been resolved. However, my other problem with all these
programs was that they're all proprietary. Those who've read more of my blog
may have realized by now that I strongly prefer free software whenever
possible.

Being unable to find any free software programs to fulfill my needs, I took a
look at the features I would need, and tried to adapt other programs to fit
those particular needs. I quickly learned that it's inconvenient at best to try
and mold generic task keeping programs into the specifics of GTD. But, it did
give me a reasonable idea of what features I needed for basic usage. It
occurred to me that it shouldn't be terribly hard to just write something of my
own. So I did.

## The solution, `App::GTD`

Introducing [`App::GTD`](https://gitlab.com/tyil/raku-app-gtd), a brand new
project written in the [Raku programming language](https://raku.org/). While
still in its early phases, it seems to be usable on a day-to-day basis for me
and another colleague. In its bare basics, it's just another to-do list, but
the commands it gives you incorporate the concepts of GTD. There's an inbox
that you fill up through the day, a list of next items to work on, and projects
to structure larger tasks in.

{{< admonition title="note" >}}
The Raku programming language used to be called the Perl 6 programming
language. They function the same, but the name was changed for various reasons
I will not get into here.
{{< / admonition >}}

This program can be installed using `zef`, though I'm planning an `ebuild` for
Gentoo (and derivatives) too. Once installed, you can use `gtd` from your
shell. Doing so without arguments will show the usage information. The most
important will be `gtd add`, `gtd next` and `gtd done`. Most of these commands
require an `id` argument. The IDs required are displayed in front of the items
when listing them with commands like `inbox` or `next`.

## Daily life with `gtd`

Once you have `gtd` installed, you don't *need* to do any configuration, as the
defaults should work fine for most people. This means you can start using it
immediately if you want to try it out!

The most common invocation will be with the `add` sub-command. Whenever
something pops up that needs doing, you add it to your inbox using it.

```txt
gtd add Buy eggs
gtd add "update cpan-raku's help command"
```

These items go to your inbox, and don't need to be long, so long as *you*
understand what you meant by it. You can see that you also don't need to use
quotes around the item you want to add. All arguments after `add` will be
joined together as a string again, but some shells may perform their magic on
certain things. This is why I quoted the second call, but not the first.

All these things that you write down like this need to be sorted out at some
point. I do this every day in the morning, before I get to my regular tasks at
work. To get started, I want to see an overview of your inbox, for which the
`inbox` sub-command is intended. Running it will give you a list of all the
items in your inbox, including their ID and the date they were added.

```txt
$ gtd inbox
[1] Buy eggs                        (2019-10-17)
[2] update cpan-raku's help command (2019-10-17)
```

Now I can go through the list, and decide which actions I should undertake
specifically. These are called "next items", and the sub-command is named
`next`. Called without arguments it will give you an overview of your next
items, but when given an ID it will move an inbox item to your list of next
items. You can optionally also specify a new name for the item, to be more
clear about what needs doing.

```txt
$ gtd next
You're all out of Next actions!

$ gtd next 1
"Buy eggs" has been added as a Next item.

$ gtd next 2 "Add usage and repo info to cpan-raku, whenever it's messaged with 'help'"
"Add usage and repo info to cpan-raku, whenever it's messaged with 'help'" has
been added as a Next item.
```

You can now see that your inbox is empty when using `inbox`, and see a list of
the next items you created with `next`.

```txt
$ gtd inbox
Your inbox is empty!

$ gtd next
[1] Buy eggs                                                                 (2019-10-17)
[2] Add usage and repo info to cpan-raku, whenever it's messaged with 'help' (2019-10-17)
```

Now all that's left is to do all the things you've created next items for. When
done, you can remove the entry from your next items using `done`. This command
also works on items in your inbox, so small tasks that require no next item(s)
can be marked as done immediately.

```txt
$ gtd done 1
"Buy eggs" has been removed from your list.

$ gtd done 2
"Add usage and repo info to cpan-raku, whenever it's messaged with 'help'" has
been removed from your list.

$ gtd next
You're all out of Next actions!
```

## Future plans

The basics are here, but there are some things I'd very much like to add. First
and foremost, I want to be have a context to add to items, and a single context
the program operates in. This way, I can more clearly separate work and
personal tasks, which now just share one global context.

Additionally, I've read about a new YouTube tutorial about using `ncurses` in
Raku, which I hope can guide me through making an `ncurses` application for
this as well. Perhaps I can find time to make a `GTK` application out of it as
well.

I've already mentioned wanting to create a Gentoo `ebuild` for the application,
but this will require packaging all the module dependencies as well. This comes
with a number of hurdles that I'm trying to iron out before starting on this
endeavor. If you are on Gentoo (or a derivative) and want to assist in any way,
please contact me.

Another thing I've taken into account when structuring the application is the
possibility for other data back-end. `gtd` is currently storing it's
information in `JSON` files in a filesystem directory, which comes with various
drawbacks. It may be beneficial to also support databases such as SQLite or
PostgreSQL. However, this is not a high priority for me right now, as it would
slow down the speed at which I can make improvements to the general program.

I hope that `App::GTD` can help others to get things done as well. The program
is all but finished, but it should be usable for people besides me and my
colleague by now. If you have any suggestions or questions about the program,
do not hesitate to seek me out!
