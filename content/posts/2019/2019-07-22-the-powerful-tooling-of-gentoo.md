---
title: "The Power(ful Tooling) of Gentoo"
date: 2019-07-22
tags:
- Gentoo
---

People often ask me for my reasons to use [Gentoo](https://gentoo.org/). Many
perceive it as a "hard" distro that takes a lot of time. While it does come
with a learning curve, I don't perceive it as particularly "hard", as the
documentation is very thorough and the community is very helpful. And the
tooling you get to maintain your system is far beyond what I've come across
with any other GNU+Linux distribution.

This blog post will highlight some of the key features I love about Gentoo.
There are certainly many more perks that I don't (yet) use, so please feel free
to inform me of other cool things that I missed.

## Configurability

One of the main reasons for preferring Gentoo is due to the ease of configuring
it to work just the way you want.

A great example for this would be with `init` choices. Many distributions only
support [systemd](https://en.wikipedia.org/wiki/Systemd) these days. As I'm not
a big fan of this particular system, I want to change this. But even asking a
question about this will get you a lot of hatred in most distribution
communities. In Gentoo, however, changing init is supported and well
documented, allowing you to pick from a range of possible inits.

### `USE` flags

One of the core concepts of Gentoo are the [`USE`
flags](https://wiki.gentoo.org/wiki/USE_flag). These allow you to easily alter
the software you're compiling to use the features you want. They can also be
used to indicate which library you would like to use to make use of a certain
feature, if there are multiple implementations available.

### `make.conf`

Like most distros that work with self-compiled packages, Gentoo has a
`make.conf` file available to specify some default arguments in to use while
compiling. Unlike most other distros, Gentoo's `make.conf` also allows for some
configuration of the `emerge` utility.

For instance, I use my `make.conf` to ensure `emerge` always asks for
confirmation before performing actions. I also ensure that the build system,
`portage`, is heavily sandboxed when building packages.

Additionally, like all configuration files in `/etc/portage`, it can be made
into a directory. In this case, all files in the directory will be loaded in
alphabetical order. This allows for easier management using tools like
[Ansible](https://www.ansible.com/).

### Ease of patching

Another feature I find very useful of Gentoo, is the ease of applying my own
patches to software. If you have a custom patch for a package that you want to
be applied, all you have to do is drop it in a directory in
`/etc/portage/patches`. The directory is should be in is the same as the
package's name the patch is intended for. For instance, I have the following
patch in `/etc/portage/patches/www-client/firefox`:

```diff
diff --git a/browser/extensions/moz.build b/browser/extensions/moz.build
index 6357998..c5272a2 100644
--- a/browser/extensions/moz.build
+++ b/browser/extensions/moz.build
@@ -5,15 +5,10 @@
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 DIRS += [
-    'activity-stream',
     'aushelper',
     'followonsearch',
     'formautofill',
     'jaws-esr',
-    'onboarding',
-    'pdfjs',
-    'pocket',
-    'screenshots',
     'webcompat',
 ]
```

Whenever a new Firefox is released and built, this patch will be applied on it
to remove some of the features I dislike.

## Ebuilds and overlays

In Gentoo vocabulary, `ebuild` files are the files that describe how a package
is to be built, which `USE` flags it supports and everything else relating to a
package. An overlay is a repository of ebuild files. Everyone can make their
own, and easily add 5 lines in their `repos.conf` to use it. In most cases,
they're just git repositories.

The documentation on everything around ebuilds is superb, in my experience,
especially compared to other distros. It is incredibly easy to get started
with, since it's made to be usable with very little effort. While being simple,
it's also very flexible: All default behaviours can be overwritten if needed to
get a package to build. 

## Binary packages

Yes, you read that right. [Binary
packages](https://wiki.gentoo.org/wiki/Binary_package_guide)! Contrary to
popular belief, Gentoo *does* support this. You can instruct `emerge` to build
binary packages of all the packages it compiles, which can then be re-used on
other systems. It does need to be compiled in such a way that the other machine
can use it, of course. You can't simply exchange the packages of an x64 machine
with and ARM machine, for instance. You can set up a [cross build
environment](https://wiki.gentoo.org/wiki/Cross_build_environment) to get that
particular usecase going, though.

If you want to easily share the binary packages you build with one machine, you
can set up a
[binhost](https://wiki.gentoo.org/wiki/Binary_package_guide#Setting_up_a_binary_package_host),
and have `emerge` pull the binary packages on the other systems as needed using
`--usepkg`. There actually is a [binhost provided by Gentoo
itself](http://packages.gentooexperimental.org/), but it seems to only contain
important packages used to restore systems into a working state.

## Tooling

Some of the core tooling available to any Gentoo user has already been talked
about. But there's some additional tooling you can install to make your life
even better.

### `genkernel`

One of the hardest tasks to newcomers to Gentoo is often to compile a kernel.
Of course, Gentoo has an answer for this, `genkernel`. The defaults `genkernel`
will give you are reasonably sane if you just want to have a kernel that works.
Of course, you can still edit the kernelconfig before compilation starts. It
will also build an `initramfs` when requested, that goes along with the kernel.
When things have been made, the kernel and initramfs will be moved to `/boot`,
and a copy of the working kernelconfig is saved to `/etc/kernels`. All you need
to remember is to update your preferred bootloader's configuration to include
your new kernel.

### `eix`

[`eix`](https://wiki.gentoo.org/wiki/Eix) is a utility most Gentoo users use to
update the Portage repositories and search for available packages. The
interface is considered more convenient, and it's a bit faster at getting your
results.

To get a quick overview of which packages are in need of updates, you can run
`eix -uc` (*u*pdates, *c*ompact). To sync the Portage tree and all overlays,
`eix-sync` is the way to go. This will ensure the cache used by `eix` also gets
updated.

In addition to having a cleaner interface and being faster, it also comes with
additional tools for keeping your system sane. The most notable to me is
`eix-test-obsolete`.

This utility will report any installed packages that are no longer provided by
any repository (orphaned packages). It will also report all configuration lines
that affect such packages. This is really valuable in keeping your
configuration maintainable.

### `glsa-check`

The `glsa-check` utility is part of the `app-portage/gentoolkit` package. When
ran, it will produce a list of all packages which have known vulnerabilities.
It will use the [GLSA database](https://security.gentoo.org/glsa) for the list
of known vulnerabilities. This can be much easier than subscribing to a mailing
list and having to check every mail to see if a vulnerability affects you.

### `qlop`

`qlop` is another utility that comes with `app-portage/gentoolkit`. This
program parses the logs from `emerge` to give provide you with some
information. I use this mostly to see compile times of certain packages using
`qlop -Htvg <package-name>`. Using this, I can more easily deduce if I want my
desktop (with a stronger CPU) to compile a certain package, or if it'll be
faster to just compile it on my laptop.
