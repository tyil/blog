---
date: 2022-03-05
title: Deprecating ReiserFS
tags:
- BTRFS
- Filesystems
- GNU+Linux
- ReiserFS
- ZFS
- bcachefs
---

[ReiserFS is getting deprecated from Linux](https://lkml.org/lkml/2022/2/20/89),
mostly due to it not being ready for the year 2038. This is a little sad, as I
still use it on some systems for storing the Gentoo Portage tree, and the Linux
kernel sources. It works well for this because it supports
[tail packing](https://en.wikipedia.org/wiki/Block_suballocation#Tail_packing),
a form of block suballocation, which can save disk space.

So, what alternatives are there for ReiserFS? After asking around and reading
some comments around the Internet, I've narrowed it down to 3 potential
candidates, bcachefs, btrfs, and zfs. Each comes with their own pros and cons,
as things tend to do.

## bcachefs

There are several downsides for bcachefs for me. The first one I found was that
the documentation on their main site seems a bit lacking, followed shortly by
finding that there are no ebuilds for it in Gentoo.

Since it was suggested several times on comments on a certain orange site, I
asked around if it at least supported block suballocation, which is the main
reason I would want to use it anyway. The answer came back as a "no", so I could
safely ignore it for the rest of the journey.

## BTRFS

BTRFS seems like a more serious contender. It supports block suballocation, and
has good enough documentation. As an additional benefit, it is supported in the
mainline Linux kernel, making it easy to use on any modern setup. There are a
few issues, such as having to rebalance in certain situations, and this
rebalancing can itself cause issues. The files I'm storing are relatively easily
recreated with a single git clone, or downloading a tarball and unpacking that,
so that doesn't have to be problematic to me.

## ZFS

The final contestant, ZFS, supports block suballocation and has great
documentation. It is not part of the mainline Linux kernel, however, so this may
make things more complex on some systems. I run ZFS already on a few machines,
but not all, so where it is not used already, it is a drawback.

Since my main concern is storing many small files, I created a few logical
volumes (and 1 ZFS subvol) and cloned the main reason for wanting a filesystem
with block suballocation, the [Gentoo Portage
tree](https://github.com/gentoo/portage). The cloning itself was done with
`--depth=1`.For reference, I also created an ext4 volume.

```
/dev/mapper/edephas0-test.btrfs     5.0G  559M  3.8G  13% /tmp/test/btrfs
/dev/mapper/edephas0-test.ext4      4.9G  756M  3.9G  17% /tmp/test/ext4
/dev/mapper/edephas0-test.reiserfs  5.0G  365M  4.7G   8% /tmp/test/reiserfs
tyilstore0/test                     5.0G  1.1G  4.0G  21% /tmp/test/zfs
```

Looking at the output from `df -h`, ReiserFS seem to still be a clear winner
when it comes to storing many small files. Nothing is even close. What does
surprise me, however, is that ZFS is actually resulting in the largest space
requirement. I'm not sure why this is, as it should support block suballocation
just fine according to [the filesystem comparison chart on
Wikipedia](https://en.wikipedia.org/wiki/Comparison_of_file_systems#Allocation_and_layout_policies).

BTRFS comes out as the next best option, after ReiserFS, so that'll be what I am
going to use on my systems for storing large trees of small files.
