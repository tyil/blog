---
date: 2022-04-23
title: A cookbook!
tags:
- Food
---

Last week, I've decided to add a cookbook to my website. I've thought about
doing so for a while, but was rather hesitent since my website was mostly a
tech-minded blog. However, this time around I've decided to simply add a
cookbook, and have seperate RSS feeds for my [blog posts](/posts/index.xml) and
[recipes](/recipes/index.xml), so that readers can decide what they want to
follow. Now I can easily share any recipes that visitors have asked me to share
over the years.

The cookbook has an overview of all recipes, and each invidual recipe has a
layout greatly inspired by MartijnBraam's [FatHub](https://fathub.org). The
JavaScript is an exact copy, even.

The format of the recipes has been altered slightly. I found the FatHub format
to be a little too verbose to make it simple enough to want to write down the
recipes. So I've trimmed down on some of that. You can check the sources that I
use on [git.tyil.nl](https://git.tyil.nl/blog/).

Since my website is generated with [Hugo](https://gohugo.io), I had to do the
template myself as well. But these were fairly simple to port over, and I am
fairly happy with the result. I've made two changes compared to the layout used
by FatHub: Every step in the instructions is numbered, and the checkboxes for
the ingredients are at the left-hand side. The ingredients don't contain a
preparation step in the tables, as I've made that part of the cooking
instructions.

The entire thing was done in an afternoon during Easter, so it was pretty
straightforward to set up. I haven't included many recipes yet, as I want to
double-check the right amounts of ingredients and instructions for many of them.
The downside of a recipe is that people generally follow it to the letter, and
I'm not a professional cook, I often cook just by tasting and adjusting as
desired.

There's one thing I didn't include, but which I might consider working on in the
future: [baker's percentages](https://en.wikipedia.org/wiki/Baker_percentage).
Some of my recipes have been written down in this notation, and for now I'll be
converting them to regular quantities instead. There's also some downsides to
baker's percentages such as harder to calculate what a serving size is, or the
duration of certain steps, which has caused me to not make this a hard
requirement for the cookbook section of my site.

I think the cookbook is more than enough to get started with, and to try sharing
some recipes in. If I've ever cooked something for you in person, and you would
like to know the recipe, don't hesitate to ask me to include it in the cookbook.
Additionally, if you have some tips on how to improve an existing recipe, I'd be
very happy to hear about it!
