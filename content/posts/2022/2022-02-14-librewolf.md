---
date: 2022-02-14
title: Trying out LibreWolf
tags:
- Firefox
- LibreWolf
---

Over the past week, I've been trying out [LibreWolf](https://librewolf.net/) as
an alternative to mainline Firefox. I generally don't hold a high opinion on any
"modern" browser to begin with, but Firefox has been the least bad for quite
some time. I used to actually like Firefox, but Mozilla has done their best to
alienate their user base in search for profits, and is eventually left with
neither. Their latest effort in digging their own grave is teaming up with Meta.

As such, I have been searching for an alternative (modern) browser for a long
time. One major requirement that I've had is to have something like
[uMatrix](https://addons.mozilla.org/en-US/firefox/addon/umatrix/). And
obviously major features to block any and all advertisements, as these are a
major detriment to your own mental health, and to the resources your machine
uses. So, when someone recommended me LibreWolf, which is just a more
user-respecting fork of Firefox, I didn't hesitate to try it out.

The migration from Firefox to LibreWolf was remarkably simple. Since I use [a
small
wrapper](https://git.tyil.nl/dotfiles/tree/.local/bin/firefox?id=1c8e9b136d9d00decc1d3570fe58072427107148)
to launch Firefox with a specific profile directory, I just had to update that
to launch LibreWolf instead. It kept all my settings, installed add-ons, and even
open tabs. It seems that by default, however, it will use its own directory for
configuration. If you want to try out LibreWolf and have a similar experience,
you can just copy over your old Firefox configuration directory to a new
location for use with LibreWolf. In hindsight, that probably would've been the
safer route for me as well, but it already happened and it all went smooth, so
no losses.

Now, while LibreWolf is more-or-less like Firefox, but less harmful to its own
users, some of the tweaks made by the LibreWolf team may or may not be desired.
I've taken note of any differences that could be conceived as issues. So far,
they're not breaking for me, but these may be of interest to you if you're
looking to try LibreWolf out as well.

## HTTP

By default, LibreWolf will not let you visit sites over HTTP. This is generally
a very nice feature, but for some public hot-spots, this may cause issues. These
are generally completely unencrypted, and LibreWolf will refuse to connect. The
page presented instead will inform you that the page you're trying to visit is
unencrypted, and allow you to make a temporary exception. Not a very big issue,
but it may be a little bit more annoying than you're used to.

## Add-ons

While all my add-ons were retained, I did want to get another add-on to redirect
me away from YouTube, to use an Invidious instance. The page for installing
add-ons itself seems to work fine, but upon clicking the Install button, and
accepting the installation, LibreWolf throws an error that it simply failed to
install anything. The Install button is nothing more than a fancy anchor with a
link to the `xpi` file, so you can manually download the file and install the
add-on manually through the [Add-ons Manager](about:addons).

## Element

I've been using Matrix for a while, as an atechnical-friendly, open source
platform, for those unwilling to use IRC. Their recommended client,
[Element](https://app.element.io/), is just another web page, because that's
sadly how most software is made these days.  The chat itself works without a
hitch, but there are two minor inconveniences compared to my regular Firefox
setup.

The first one is that LibreWolf does not share my local timezone with the
websites I visit. This causes timestamps to be off by one hour in the Element
client. A very minor issue that I can easily live with.

The other is that the "default" icons, which is a capital letter with a colored
background, [don't look so well](https://dist.tyil.nl/blog/matrix-icons.png).
There's some odd artifacts in the icons, which doesn't seem to affect the letter
shown. Since I mostly use the
[weechat-matrix](https://github.com/poljar/weechat-matrix) plugin, it's not
really an issue. And for the few times I do use Element, it doesn't bother me
enough to consider it a real issue.

## Jellyfin

For consuming all sorts of media, I have [Jellyfin](https://jellyfin.org/) set
up for personal use. This worked fine in my regular Firefox setup, but does not
seem to be willing to play any videos in LibreWolf. The console logs show some
issues with websockets, and I've not been able to find a good way to work around
this yet. For now, I'll stick to using `mpv` to watch any content to deal with
this issue.

All in all, I think LibreWolf is a pretty solid browser, and unless I discover
something major to turn me off, I'll keep using it for the foreseeable future.
