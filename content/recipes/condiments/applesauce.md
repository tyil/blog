---
title: Applesauce
date: 2022-04-18
tags:
- condiments
- sweet
preptime: 75
cooktime: 10
serves: 1

ingredients:
- label: Apple
  amount: 5
- label: Water
  unit: liter
  amount: 100
- label: Cinnamon

stages:
- label: Preparation
  steps:
  - Peel the apples.
  - Cut the apples into small chunks.
- label: Cooking
  steps:
  - Put the apples in the pot.
  - Add water to the pot, just enough to cover about half of the apple.
  - Put the pot on the stove, and turn it on to high heat.
  - When the water starts bubbling, lower the heat to about a third, and cover
    the pot with a lid kept slightly ajar.
  - Let this cook for about 10 to 15 minutes.
  - Once cooked, drain the water from the pot.
  - Mash the apples to your preferred thickness.
  - Add cinnamon to taste.
  - Let cool completely in the fridge.
---

A sweet, thick sauce made of apple.
