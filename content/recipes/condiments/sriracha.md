---
title: Sriracha Sauce
date: 2022-04-18
tags:
- condiments
- spicy
preptime: 10
cooktime: 432000
serves: 1

ingredients:
- label: Peppers
- label: Garlic
- label: Sugar
- label: Salt

stages:
- label: Preperation
  steps:
  - Cut the peppers in chunks that will easily blend
  - Weigh all the ingredients without the salt
  - Take 3% of the total weight as salt, and add it to the rest of the ingredients
  - Blend it all together until smooth
- label: Fermentation
  steps:
  - Let ferment for 5 - 30 days
---

A very spicy variant of the world's most popular hot sauce.
