---
title: Jordanese Meatball
date: 2022-04-18
preptime: 20
cooktime: 270
serves: 1
tags:
- meal
- hot

ingredients:
- label: Minced meat
  ratio: 1
  amount: 150
  unit: grams
- label: Egg
  ratio: 0.05
  amount: 7.5
  unit: grams
- label: Garlic
- label: Onion
- label: Salt
  ratio: 0.012
  amount: 18
  unit: grams
- label: Black Pepper
  ratio: 0.004
  amount: 1
  unit: grams
- label: Nutmeg
  ratio: 0.002
  amount: 0.5
  unit: grams
- label: Panko
  ratio: 0.08
  amount: 12
  unit: grams
- label: Cayenne Pepper
  ratio: 0.002
  amount: 0.5
  unit: grams

stages:
- label: Pre-cook
  steps:
  - Sweat the garlic and onion
  - Set the garlic and onion aside to cool off
- label: Shaping
  steps:
  - Toss all the other ingredients into a mixing bowl
  - Mix together
  - Add the cool onion and garlic
  - Mix together
  - Form the meat into balls
  - Cover the meatballs in panko
- label: Cooking
  steps:
  - Fry the meatballs to ensure a crust forms outside of it (10 min)
  - Fill a dutch oven with enough (meat) stock to almost fully submerge the
    meatballs
  - Let the stock thicken up slightly
  - Put in the meatballs, making sure they are submerged.
  - Cook the meatballs slowly until completely cooked through.
---

A slowly stewed meatball.
