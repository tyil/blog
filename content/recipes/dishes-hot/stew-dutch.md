---
title: Dutch Stew
draft: true
date: 2022-05-22
preptime: 30
cooktime: 450
serves: 4
tags:
- Dutch
- beef
- hot
- meal

ingredients:
- label: Beef
  amount: 500
  unit: grams
- label: Sweet onion
  amount: 2 (large)
- label: Garlic
  amount: 3
  unit: teaspoons
- label: Leek
  amount: 1
- label: Beans
  amount: 200
  unit: grams
- label: Carrot
  amount: 300
  unit: grams
- label: Chick peas
  amount: 200
  unit: grams
- label: Mushrooms
  amount: 200
  unit: grams
- label: Oudewijvenkoek
  amount: 3
  unit: slices
  links:
  - https://nl.wikipedia.org/wiki/Oudewijvenkoek
- label: Beef stock
  amount: 750
  unit: milliliters
- label: Dark beer
  amount: 330
  unit: milliliters
- label: Appelstroop
  amount: 2
  unit: tablespoons
  links:
  - https://nl.wikipedia.org/wiki/Appelstroop
- label: Mustard
  amount: 3
  unit: teaspoons
- label: Bay leaf
  amount: 4
- label: Pepper
- label: Salt
- label: Smoked paprika
- label: Thyme

stages:
- label: Prep
  steps:
  - Cut the beef into bite-sized chunks
  - Cut all the vegetables into chunks, about 1cm in diameter where applicable
  - Slice off 3 slices of the oudewijvenkoek, about 1cm thick
  - Spread the mustard unto one side of the oudewijvenkoek slices
- label: Searing
  steps:
  - Sear the beef on all sides
  - Remove the beef from the pot
- label: Stewing
  steps:
  - Saute the onions in the pot
  - Add in the garlic, cook for about half a minute
  - Add the leek to the pot, and cook for a minute
  - Add the carrot to the pot
  - Add the beer and beef stock to the pot
  - Add the bay leaves to the pot
  - Add the appelstroop to the pot
  - Stir everything together
  - Add the mustard-laced oudewijvenkoek, with the mustard facing down in the
    pot
  - Let this stew for about 6 hours, occasionally checking in to make sure its
    simmering slowly. If too much liquid evaporates, you can add more water or
    beef stock, the solids should be completely submerged
  - Add the spices (pepper, salt, smoked paprika, thyme) to reach your desired
    flavour
  - Add the potatoes, chickpeas, and beans to the pot
  - Let it stew for another 90 - 120 minutes
---

A hearthy and veggie-heavy stew, best served when the weather outside is cold
and wet.
