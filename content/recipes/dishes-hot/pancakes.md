---
draft: true
title: Pancakes
date: 2022-04-18
preptime: 5
cooktime: 15
serves: 3
tags:
- meal
- hot

ingredients:
- label: Egg
  amount: 1
- label: Flour
  amount: 150
  unit: grams
- label: Milk
  amount: 250
  unit: milliliters
- label: Bacon
- label: Cinnamon

stages:
- label: Batter
  steps:
  - Put the eggs into the mixing bowl.
  - Use the mixer to stir them into a homogenous liquid, for about 30 to 45 seconds.
  - Add the milk, and mix together until homogenous again, about 5 to 10 seconds.
  - Add the flour, and mix into a smooth batter, about 1 to 3 minutes.
- label: Cooking
  steps:
  - Get your pan as hot as you can.
  - Add a little butter (or oil) to the pan, and ensure it covers the entire pan slightly.
  - Put some strips of bacon in the pan.
  - Immediately after, add 1 scoop of pancake batter. Start pouring over the
    bacon, then swirl around the pan to spread the batter around the surface of
    the pan.
  - Let the pancake cook until the top side is solid.
  - Flip the pancake.
  - Let cook for another 30 to 60 seconds.
  - Rinse and repeat until you're out of pancake batter.
---

Thin, Dutch pancakes with bacon. To be eaten with the gift of the Dutch Gods,
known as [Stroop](https://nl.wikipedia.org/wiki/Stroop).
