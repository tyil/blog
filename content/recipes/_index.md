---
title: Cookbook
---

People have often asked me to share recipes for various meals and snacks I've
served over time, so I've started writing down my recipes. This section of my
blog covers these recipes in my own personal cookbook. If you want to stay up to
date with the latest additions, subscribe to the [RSS
feed](/recipes/index.xml).
