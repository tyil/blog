---
title: Bechamel
date: 2022-04-18
preptime: 0
cooktime: 10
serves: 1
tags:
- basics

ingredients:
- label: Butter
  amount: 50
  unit: grams
- label: Flour
  amount: 60
  unit: grams
- label: Milk
  amount: .25
  unit: liter

stages:
- label: Cooking
  steps:
  - Put the pot on low heat.
  - Add the butter to the pot and let it melt completely.
  - Once completely melted, immediately add the flour.
  - Keep on low heat, and continuously stir the mixture to avoid burning.
  - After 3 - 5 minutes, the mixture will smell slightly nutty, indicating the
    flour is cooked.
  - While stirring, slowly add the milk to the mixture.
  - Let this cook for 3 - 5 minutes, until you reach your desired viscosity.
---

A thick, creamy sauce, usually as a base for another thick and savory sauce.
