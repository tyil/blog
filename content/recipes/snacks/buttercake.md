---
title: Buttercake
date: 2022-04-18
tags:
- snacks
- rich
- sweet
preptime: 15
cooktime: 30
serves: 8

ingredients:
- label: Egg
  amount: 1
- label: Butter
  amount: 200
  unit: grams
- label: Flour
  amount: 250
  unit: grams
- label: Granulated Sugar
  amount: 200
  unit: grams
- label: Vanilla Sugar
  amount: 8
  unit: grams

stages:
- label: Preparations
  steps:
  - Ensure the butter is on room temperature
  - Heat up the oven to 180℃
- label: Batter
  steps:
  - Whip the butter until its nice and soft
  - Add in the sugars, and mix until combined
  - Add in an egg, and mix until combined
  - Sift in the flour, and mix until combined
- label: Shaping
  steps:
  - Butter your cake tin
  - Put the cake batter into the cake tin
  - Smooth out the cake batter, using a wet spoon
  - Using a fork, carve a diamond pattern on the cake batter
  - Put your 2nd egg in a small cup, and whisk into a single cohesive substance
  - Lightly coat the cake with egg
- label: Baking
  steps:
  - Bake in the oven for 25-30 minutes
  - Let it cool to room temperature, preferably leaving it overnight until serving
---

A rich snack from the glorious Netherlands.
