---
title: Kruidnoten
date: 2022-04-18
tags:
- snacks
preptime: 60
cooktime: 15
serves: 2

ingredients:
- label: Baking Powder
  amount: 3
  unit: grams
- label: Bastard Sugar (Dark)
  amount: 60
  unit: grams
- label: Butter
  amount: 70
  unit: grams
- label: Flour
  amount: 100
  unit: grams
- label: Milk
  amount: 25
  unit: grams
- label: Speculaas spices
  amount: 6
  unit: grams
- label: Vanilla Extract
  amount: 3
  unit: grams

stages:
- label: Batter
  steps:
  - Put all ingredients in a mixing bowl
  - Use a mixer with dough hooks to combine everything into a cohesive dough
  - Take the dough out of the mixing bowl, and wrap it in plastic wrap
  - Leave the wrapped dough in the fridge for 30 - 45 minutes to rest
- label: Baking
  steps:
  - Take the dough out of the fridge
  - Turn on the oven to 448K
  - Take small bits of dough, and roll them into balls
  - Put the dough balls on a baking sheet
  - Bake the dough balls for 15 minutes
  - Take the kruidnoten out of the oven, and let cool on a wire rack for 15 - 30 minutes
---

A Dutch snack for Sinterklaas, but very tasty at any time of the year.
