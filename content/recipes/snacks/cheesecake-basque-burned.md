---
title: Basque-burned Cheesecake
date: 2022-04-18
tags:
- snacks
- sweet
preptime: 75
cooktime: 10
serves: 1

ingredients:
- label: Cream cheese
  amount: 900
  unit: grams
- label: Egg
  amount: 500
  unit: grams
- label: Flour
  amount: 40
  unit: grams
- label: Granulated sugar
  amount: 300
  unit: grams
- label: Heavy cream
  amount: 550
  unit: grams
- label: Salt
  amount: 1
  unit: teaspoon
- label: Vanilla extract
  amount: 1
  unit: tablespoon

stages:
- label: Preparation
  steps:
  - Ensure all the ingredients are at room temperature.
  - Turn on the oven to 477K
- label: Batter
  steps:
  - Put the cream cheese into the mixing bowl.
  - Put the sugar into the mixing bowl.
  - On a low speed, mix the cream cheese and sugar together into a single, soft mixture.
  - Add in one egg, and mix until combined, repeat until all eggs have been mixed in.
  - Add the vanilla extract and salt, and mix until combined.
  - Sift in the flour, and mix until combined.
  - Grease up the mixing bowl, to make it slightly sticky for the baking sheet.
- label: Shaping
  steps:
  - Put the baking sheet into the mixing bowl, it does *not* need to look pretty!
  - Ensure the baking sheet sticks out of the cake tin, along the sides, as the
    cheesecake itself will rise way above the cake tin's height.
  - Pour the cake batter from the mixing bowl into the cake tin.
- label: Baking
  steps:
  - Bake the cheesecake for about 60 minutes.
  - Let the cheesecake cool to room temperature.
  - Refridgerate the cheesecake for at least 2 hours.
---

The easiest cheesecake you've ever made. Baked quickly and at a high temperature
without regard for looking pretty.
