---
title: About Me
---

Freedom-focused hacker. I love everything about free software, and spend much
time contributing to it myself. I occasionally write blog posts to help other
people getting started with free software, show the issues with proprietary
software, or just talk about something I find fun and interesting.

Pretty much all of my projects can be found on
[git.tyil.nl](https://git.tyil.nl), though many of them are also mirrored on
other code forges.

I can be reached through various means of communication, though all of them
usable by completely free means. Scroll down to the **Channels** section for
more information about that.

## Contact

### PGP

My public PGP key is available [from my own site][pubkey], or from a public key
server such as [pgp.mit.edu][pubkey-mit]. The fingerprint is:

    1660 F6A2 DFA7 5347 322A  4DC0 7A6A C285 E2D9 8827

You can also fetch my PGP key using the
[WebKey Protocol](/post/2020/05/30/setting-up-pgp-wkd/):

    gpg --locate-key p.spek@tyil.nl

[pubkey]: /pubkey.txt
[pubkey-mit]: http://pgp.mit.edu/pks/lookup?op=vindex&search=0x7A6AC285E2D98827

### Channels

####  Email

Email contact goes via [p.spek@tyil.nl][mail]. Be sure to at least sign all
mail you send me. Even better would be encrypted mail using my [PGP
key][pubkey].

I do not read my mailboxes very often, so please do not expect a timely
response. If you require a response as soon as possible, please find me on IRC
instead.

#### IRC

I am active on various IRC networks, most often under the nick `tyil`. All of
these are connected from the same client, so you can pick any of these if you
wish to have a real-time chat with me.

- [DareNET](https://darenet.org)
- [Libera](https://libera.chat)
- [OFTC](https://www.oftc.net/)
- [Rizon](https://rizon.net)

#### Matrix

While I'm not fully convinced of Matrix yet, I have an active account on the
network. I'd like to eventually self-host a homeserver, but for that to happen I
would first like it to be actually good. For now, you can find me as
`@tyil:matrix.org`.

## Other links

- [Sourcehut account][git-srht]
- [GitLab account][git-gl]
- [GitHub account][git-gh]

## RSS

If you'd like to stay up-to-date with my posts, you can subscribe to the [RSS
feed](/posts/index.xml).

[git-gh]: https://github.com/tyil
[git-gl]: https://gitlab.com/tyil
[git-srht]: https://sr.ht/~tyil/
[mail]: mailto:p.spek@tyil.nl
[pubkey]: /pubkey.txt
