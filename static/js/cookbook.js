"use strict";

// Kindly copied from MartijnBraam's fathub sources
// https://sr.ht/~martijnbraam/fathub.org/

var serve_fraction = 1.0;

function update_ingredient_list() {
    var ingredients = document.querySelectorAll('td[data-amount]');
    for (var i = 0; i < ingredients.length; i++) {
        var ingredient = ingredients[i];
        ingredient.innerText = ingredient.dataset.amount * serve_fraction;
    }
}

function adjust_serves() {
    serve_fraction = this.value / this.dataset.original;
    update_ingredient_list();
}

function add_dynamic_controls() {
    var serves = document.querySelector("td[data-serves]");
    var spinner = document.createElement('INPUT');
    spinner.type = 'number';
    spinner.dataset.original = serves.dataset.serves;
    spinner.value = serves.dataset.serves;
    spinner.addEventListener('change', adjust_serves);
    serves.innerHTML = '';
    serves.appendChild(spinner);
}

document.addEventListener("DOMContentLoaded", function () {
    add_dynamic_controls();
});
